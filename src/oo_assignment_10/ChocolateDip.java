package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class ChocolateDip extends Topping {

    public ChocolateDip( Ice ice ) {
        super( ice );
        if( ice instanceof WhippedCream ) {
            throw new IllegalArgumentException( "Cannot add chocolate dip to ice cream that already has whipped cream." );
        }
        
    }

    @Override
    public String giveDescription() {
        return ice.giveDescription() + ", chocolate dip";
    }

    @Override
    public int price() {
        return super.price() + 30;
    }
}
