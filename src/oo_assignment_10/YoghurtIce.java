package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class YoghurtIce extends Ice {
    
    public YoghurtIce( ) {
        this.description = "Yoghurt ice";
    }


    @Override
    public int price() {
        return 200;
    }

}
