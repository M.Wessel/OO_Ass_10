package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public abstract class Topping extends Ice {
    
    protected final Ice ice;
    
    public Topping( Ice ice ){
        this.ice = ice;
    }
    
    @Override
    public int price() {
        return ice.price();
    }
    
    @Override
    public String giveDescription() {
        return ice.giveDescription();
    }
    
}
