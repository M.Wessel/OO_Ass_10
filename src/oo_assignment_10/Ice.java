package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public abstract class Ice {
    
    protected String description = "Unknown ice";
    
    /**
     * Prints description to the user.
     */
    public String giveDescription() {
        return description;
    }

    /**
     * Returns the price in cents.
     *
     * @return
     */
    public abstract int price();

}
