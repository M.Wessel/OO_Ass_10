package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class VanillaIce extends Ice {

    public VanillaIce() {
        this.description = "Vanilla ice";
    }
    
    @Override
    public int price() {
        return 150;
    }

}
