package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class WhippedCream extends Topping{
    public WhippedCream( Ice ice ) {
        super( ice );
    }
    
    @Override
    public String giveDescription( ){
        return ice.giveDescription() + ", sprinkles" ;
    }
    
    @Override
    public int price() {
        return super.price() + 50;
    }
}
