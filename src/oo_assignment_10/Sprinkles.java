package oo_assignment_10;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Sprinkles extends Topping {
    
    public Sprinkles( Ice ice ) {
        super( ice );
    }
    
    @Override
    public String giveDescription( ){
        return ice.giveDescription() + ", sprinkles";
    }
    
}
