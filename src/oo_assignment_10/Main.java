package oo_assignment_10;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main( String[] args ) {

        List<Ice> iceCream = new LinkedList<>();
        iceCream.add( new WhippedCream( new ChocolateDip( new VanillaIce() ) ));
        iceCream.add( new WhippedCream( new WhippedCream( new YoghurtIce() ) ));
        iceCream.add( new Sprinkles( new ChocolateDip ( new Sprinkles( new YoghurtIce() ) )));
        iceCream.add( new WhippedCream( new ChocolateDip( new Sprinkles( new VanillaIce() ) )));
        
        for ( Ice i : iceCream ) {
            System.out.println( i.giveDescription() + "  " + i.price() + " cents");
        }
        
        
        // Ice that should not be made:
//        Ice fancyIce = new ChocolateDip( new WhippedCream( new ChocolateDip( new VanillaIce() ) ) );
//        System.out.println( fancyIce.price() );
//        fancyIce.giveDescription();

    }

}
