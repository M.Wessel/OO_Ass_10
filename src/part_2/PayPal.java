package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class PayPal implements PaymentMethod
{
    private String email;
    private String password;
    
    /**
     * A constructor setting all the details to "null"
     */
    public PayPal()
    {
        email = null;
        password = null;
    }

    /**
     * A setter for the account details
     * @param e - email
     * @param p - password
     */
    public void setDetails(String e, String p)
    {
        email = e;
        password = p;
    }
    
    @Override
    public boolean pay(double amount)
    {
        System.out.println("Paying with PayPal:");
        if (email!=null && password!=null)
        {
            System.out.println("All is well, payment completed!");
            return true;
        }
        else
        {
            System.out.println("Payment failed, incorrect details");
            return false;
        }
    }
}
