package part_2;

import java.util.ArrayList;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class ShoppingCart
{
    ArrayList<Item> items;
    PaymentMethod pm;
    
    /**
     * A constructor, creating an empty shopping cart
     */
    public ShoppingCart()
    {
      items = new ArrayList<Item>();
      pm = new iDeal();
    }
    
    /**
     * A method to add an item to the shopping cart
     * @param item 
     */
    public void add(Item item)
    {
        items.add(item);
    }
    
    /**
     * A method to remove an item from the shopping cart
     * @param item 
     */
    public void remove(Item item)
    {
        items.remove(item);
    }
    
    /**
     * Computes the total price of all the items in the shopping cart, including
     * shipping costs. Items with the same shipping cost are dispatched together,
     * and so the shipping cost is only incurred once.
     * @return double - the total price to be paid by the customer
     */
    public double computeTotal()
    {
        double total = 0.0;
        ArrayList<Double> shippingCosts = new ArrayList<>();
        
        for (Item item:items)
        {
            total += item.getPrice();
            double sc = item.shippingCost();
            if (!shippingCosts.contains(sc))
            {
                total += sc;
                shippingCosts.add(sc);
            }
        }
        
        return total;
    }
    
    /**
     * Computes the total price and calls the payment method's "pay" method
     * @return true if the payment was successful, false if it wasn't
     */
    public boolean pay()
    {
        double total = computeTotal();
        System.out.println("Going to payment. The total price is " + total + ".");
        return pm.pay(total);
    }
    
    /**
     * A setter for the payment method
     * @param paymentMethod 
     */
    public void changePaymentMethod(PaymentMethod paymentMethod)
    {
        pm = paymentMethod;
    }

    /**
     * A fun little method that describes all the items in the shopping cart
     * @return string - A "list" of descriptions
     */
    public String overview()
    {
        StringBuilder s = new StringBuilder("You have selected:\n");
        for (Item item:items)
        {
            s.append(item.getDescription() + "\n");
        }
        return s.toString();
    }
}
