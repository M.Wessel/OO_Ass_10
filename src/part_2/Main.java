package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Main
{
    /**
     * The main method, showcasing the functionality of the program :) 
     * @param args 
     */
    public static void main(String[] args)
    {
        System.out.println("Welcome to our website. You have been assigned an "
                + "empty shopping cart! Go ahead and get shoppin'!");
        ShoppingCart sc = new ShoppingCart();
        sc.add(new Watermelon());
        sc.add(new WineGlass());
        sc.add(new WashingMachine());
        System.out.println(sc.overview());
        System.out.println("It seems you wish to pay by creditcard. We shall "
                + "enter your credit card details for you!");
        CreditCard cc = new CreditCard();
        cc.setDetails(12345678, 12032020);
        sc.changePaymentMethod(cc);
        sc.pay();
        System.out.println("Thank you for visiting our website, come again!");
    }
}
