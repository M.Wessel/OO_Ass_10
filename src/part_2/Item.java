package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public abstract class Item
{
    protected String description;
    protected double price;
    
    /**
     * A method to retrieve the shipping cost of an item
     * @return double: the item's shipping cost
     */
    abstract public double shippingCost();
    
    /**
     * A getter for the description of the item
     * @return description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * A getter for the price of the item
     * @return price
     */
    public double getPrice()
    {
        return price;
    }
}
