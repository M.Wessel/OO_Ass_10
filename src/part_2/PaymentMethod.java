package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public interface PaymentMethod
{
    /**
     * A method that completes a payment using a payment method
     * @param amount - the money to be paid
     * @return true if the payment was successful, false otherwise
     */
    public boolean pay(double amount);
}
