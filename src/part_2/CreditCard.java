package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class CreditCard implements PaymentMethod
{
    private int cardNumber;
    private int expDate;
    
    /**
     * A constructor setting all the details to "null"
     */
    public CreditCard()
    {
        cardNumber = 0;
        expDate = 0;
    }
    
    /**
     * A setter for the card details
     * @param cn - card number
     * @param ed - expiration date
     */
    public void setDetails(int cn, int ed)
    {
        cardNumber = cn;
        expDate = ed;
    }
    
    @Override
    public boolean pay(double amount)
    {
        System.out.println("Paying with CreditCard:");
        if (cardNumber!=0 && expDate!=0)
        {
            System.out.println("All is well, payment completed!");
            return true;
        }
        else
        {
            System.out.println("Payment failed, incorrect details");
            return false;
        }
    }
}
