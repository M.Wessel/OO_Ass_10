package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class WineGlass extends Item
{
    public WineGlass()
    {
        description = "A type of glass that is used to drink and taste wine.";
        price = 8.50;
    }
    
    @Override
    public double shippingCost()
    {
        return 6.75;
    }
}
