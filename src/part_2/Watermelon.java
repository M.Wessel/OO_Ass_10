package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class Watermelon extends Item
{
    public Watermelon()
    {
        description = "A special kind of berry with a hard rind and no internal "
                + "division, botanically called a pepo. The sweet, juicy flesh "
                + "is usually deep red to pink, with many black seeds, although "
                + "seedless varieties have been cultivated.";
        price = 4.50;
    }
    
    @Override
    public double shippingCost()
    {
        return 6.75;
    }
}
