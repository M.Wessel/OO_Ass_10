package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class WashingMachine extends Item
{
    public WashingMachine()
    {
        description = "A device used to wash laundry.";
        price = 499.0;
    }

    @Override
    public double shippingCost()
    {
        return 30.0;
    }
}
