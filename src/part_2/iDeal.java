package part_2;

/**
 *
 * @author Yvonne van den Broek (s4828828) & Maaike Wessel (s4804473)
 */
public class iDeal implements PaymentMethod
{
    private String bank;
    private int accountNumber;
    private int PIN;
    
    /**
     * A constructor setting all the details to "null"
     */
    public iDeal()
    {
        bank = null;
        accountNumber = 0;
        PIN = 0;
    }
            
    /**
     * A setter for the account details
     * @param b - bank
     * @param an - account number
     * @param pin  - pin number
     */
    public void setDetails(String b, int an, int pin)
    {
        bank = b;
        accountNumber = an;
        PIN = pin;
    }

    @Override
    public boolean pay(double amount)
    {
        System.out.println("Paying with iDeal:");
        if (bank!=null && accountNumber!=0 && PIN!=0)
        {
            System.out.println("All is well, payment completed!");
            return true;
        }
        else
        {
            System.out.println("Payment failed, incorrect details");
            return false;
        }
    }
}
